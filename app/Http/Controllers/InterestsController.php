<?php
// пока толстый контроллер, не вынес еще в сервис
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Interest;
use Illuminate\Support\Facades\DB;
class InterestsController extends Controller
{

    public function getList(Request $request)
    {
        return Interest::select('user_interests.id','comment','lastname','firstname','status', 'user_id', DB::raw("DATE_FORMAT(user_interests.created_at,'%M %d %Y %k:%i') as date"))
            ->leftJoin('users', 'user_id', '=', 'users.id')
            ->orderBy('user_interests.created_at','desc')
            ->paginate(15);
    }

    public function delete(Interest $interest)
    {
        $interest->delete();
        return response()->json(['data' => ['status' => 'ok']]);
    }

    public function update(Interest $interest, Request $request)
    {
        $data = $request->all();
        $interest->comment = $data['comment'];
        $interest->status = $data['status'];
        $interest->save();
        return response()->json(['data' => ['status' => 'ok']]);
    }
    public function create(Request $request)
    {
        $data = $request->all();
        $interest = new Interest();
        $interest->user_id = $data['user_id'];
        $interest->comment = $data['comment'];
        $interest->status = $data['status'];
        $interest->save();
        return response()->json(['data' => ['status' => 'ok']]);
    }
}
