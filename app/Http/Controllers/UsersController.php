<?php

namespace App\Http\Controllers;

use App\Service\Users\UsersServiceIface;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\UpdateUserRequest;
class UsersController extends Controller
{
    protected $usersService;

    public function __construct(UsersServiceIface $usersService)
    {
        $this->usersService = $usersService;
    }

    public function getUsersList()
    {
        Return  $this->usersService->getList();
    }
    public function deleteUser(User $user)
    {
        $this->usersService->delete($user);
        return response()->json(['data' => ['status' => 'ok']]);
    }
    public function updateUser(User $user, UpdateUserRequest $request)
    {
        $this->usersService->create($user, $request->validated());
        return response()->json(['data' => ['status' => 'ok']]);
    }
    public function createUser(Request $request)
    {
        $this->usersService->create($request->all());
        return response()->json(['data' => ['status' => 'ok']]);
    }
}
