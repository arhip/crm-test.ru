<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|alpha-dash',
            'firstname' => 'required|alpha-dash',
            'middlename' => 'required|alpha-dash',
            'phone' => 'required|digits_between:7,13',
            'email' => 'required|email'

        ];
    }
}
