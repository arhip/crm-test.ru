<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestStatus extends Model
{
    public $table = 'user_interests_statuses';
    public $timestamps = true;


}
