<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
    public $table = 'user_interests';
    public $timestamps = true;
    protected $fillable = ['comment', 'status'];
}
