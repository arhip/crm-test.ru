<?php

namespace App\DataEntities;

class BasicEntity
{
    protected $relationships = [];
    protected $meta = [];

    public function addSimpleRelationship(string $key, $relationship)
    {
        $this->relationships[$key] = $relationship;
    }

    public function delRelationship(string $key)
    {
        unset($this->relationships[$key]);
        return $this;
    }

    public function addArrayRelationships(string $key, $relationship)
    {
        $this->relationships[$key] = $this->relationships[$key] ?? [];
        $this->relationships[$key][] = $relationship;
    }

    public function getRelationship(string $key)
    {
        return $this->relationships[$key] ?? null;
    }

    public function addMeta(string $key, $metaInfo)
    {
        $this->meta[$key] = $metaInfo;
    }

    public function getMeta(string $key)
    {
        return $this->meta[$key] ?? null;
    }

    public function getAttributes()
    {
        $attributes = [];
        foreach(static::COLUMNS as $key) {
            $attributes[$key] = $this->{$key};
        }
        return $attributes;
    }

    public function toArray()
    {
        $array = [];
        $array['attributes'] = $this->getAttributes();
        if(count($this->relationships) > 0){
            $array['relationships'] = [];
            foreach($this->relationships as $relKey => $relEntity){
                if(!isset($relEntity)){
                    $array['relationships'][$relKey] = null; continue;
                }
                if(is_array($relEntity)){
                    $array['relationships'][$relKey] = array_map(function($item){ return $item->toArray(); }, $relEntity);
                    continue;
                }
                $array['relationships'][$relKey] = $relEntity->toArray();
            }
        }
        if(count($this->meta) > 0){
            $array['meta'] = $this->meta;
        }
        return $array;
    }
}
