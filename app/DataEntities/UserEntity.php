<?php

namespace App\DataEntities;

class UserEntity extends BasicEntity implements EntityIFace
{
    const COLUMNS = ['id', 'phone', 'firstname', 'lastname', 'middlename', 'email', 'created_at', 'updated_at'];
    const EDITABLE_COLS = ['phone','firstname', 'lastname', 'middlename', 'email'];

    public $id;
    public $phone;
    public $firstname;
    public $lastname;
    public $middlename;
    public $email;
    public $created_at;
    public $updated_at;

}

?>
