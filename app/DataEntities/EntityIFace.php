<?php

namespace App\DataEntities;

interface EntityIFace
{
    public function addSimpleRelationship(string $key, $relationship);
    public function addArrayRelationships(string $key, $relationship);
    public function getRelationship(string $key);
    public function addMeta(string $key, $metaInfo);
    public function getMeta(string $key);
    public function toArray();
}