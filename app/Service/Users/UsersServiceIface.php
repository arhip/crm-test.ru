<?php

namespace App\Service\Users;


use App\Models\User;

interface UsersServiceIface
{
    public function create(array $data);
    public function update(User $user, array $data);
    public function delete(User $user);
    public function getList();
}