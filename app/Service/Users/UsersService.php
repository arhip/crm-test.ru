<?php

namespace App\Service\Users;

use App\DataEntities\UserEntity;
use App\Models\User;
use App\RealRepositories\Ifaces\UsersRepoIface;

class UsersService implements UsersServiceIface
{
    protected $usersRepo;

    public function __construct(UsersRepoIface $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }


    public function create(array $data){
        $entity = new UserEntity();

        foreach($data as $key => $val){
            $entity->{$key} = $val;
        }
        $this->usersRepo->add($entity);
    }
    public function update(User $user,array $data){
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->middlename = $data['middlename'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        $user->save();
    }
    public function delete(User $user){
        $this->usersRepo->del([$user->id]);
    }
    public function getList(){
        Return User::paginate(15);
    }
}