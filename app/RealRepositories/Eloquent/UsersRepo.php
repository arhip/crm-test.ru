<?php

namespace App\RealRepositories\Eloquent;

use App\Models\User;
use App\DataEntities\{UserEntity};//EntitiesRelations
use App\RealRepositories\Ifaces\UsersRepoIface;

class UsersRepo extends AbstractRepo implements UsersRepoIface
{
    const ENTITY_CLASS = UserEntity::class;
    const DB_TABLE = 'users';
    const ELOQUENT_CLASS = User::class;

    const ELOQUENT_RELATIONS = [    ];

    public function get(int $id) : ?UserEntity
    {
        return $this->getEntity($id);
    }

    public function add(UserEntity $entity) : UserEntity
    {
        return $this->addEntity($entity);
    }
}
