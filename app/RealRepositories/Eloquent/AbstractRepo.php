<?php

namespace App\RealRepositories\Eloquent;

use App\DataEntities\{EntitiesRelations, EntityIFace};
use Illuminate\Support\Facades\DB;


abstract class AbstractRepo
{
    protected $with = [];

    public function with(string $relationship)
    {

        if(!isset(static::ELOQUENT_RELATIONS[$relationship])){
            throw new \Exception('No such entity relationship!');
        }
        $this->with[] = $relationship;
        return $this;
    }

    public function del(array $ids)
    {
        $this->delEntities($ids);
    }

    public function all() : array
    {
        $eloquentClass = static::ELOQUENT_CLASS;
        $models = $eloquentClass::with($this->getEloquentRelations())->get();
        if($models->count() === 0){
            return [];
        }
        $result = [];
        foreach($models as $model){
            $result[] = $this->makeEntityFromEloquent($model);
        }

        return $result;
    }

    protected function getEntity(int $id) : ?EntityIFace
    {
        $eloquentClass = static::ELOQUENT_CLASS;
        $model = $eloquentClass::where('id', $id)->with($this->getEloquentRelations())->first();
        if(!isset($model)){
            return null;
        }
        return $this->makeEntityFromEloquent($model);
    }

    protected function makeEntityFromEloquent($eloquentModel) : EntityIFace
    {
        $entityClass = static::ENTITY_CLASS;
        $entity = new $entityClass;
        foreach($entityClass::COLUMNS as $key){
            $entity->{$key} = $eloquentModel->{$key};
        }
        $this->addRelationships($entity, $eloquentModel);
        return $entity;
    }

    protected function addEntity(EntityIFace $entity) : EntityIFace
    {
        $eloquentClass = static::ELOQUENT_CLASS;
        $model = isset($entity->id) ? $eloquentClass::find($entity->id) : new $eloquentClass();
        $entityClass = static::ENTITY_CLASS;
        foreach($entityClass::EDITABLE_COLS as $key){
            $model->{$key} = $entity->{$key};
        }
        $model->save();
        foreach($entityClass::COLUMNS as $key){
            $entity->{$key} = $model->{$key};
        }
        return $entity;
    }

    protected function delEntities(array $ids)
    {
        $eloquentClass = static::ELOQUENT_CLASS;
        $eloquentClass::whereIN('id', $ids)->delete();
    }

    protected function getEloquentRelations()
    {
        $relations = [];
        foreach($this->with as $relKey){
            $relations[] = static::ELOQUENT_RELATIONS[$relKey]['name'];
        }
        return $relations;
    }

    protected function addRelationships(EntityIFace $entity, $eloquentModel)
    {
        foreach($this->with as $relationKey){
            if(isset(static::ELOQUENT_RELATIONS[$relationKey])){
                $relationData = $eloquentModel->{static::ELOQUENT_RELATIONS[$relationKey]['name']};
                if(!isset($relationData) ||
                    ($relationData instanceof \Illuminate\Database\Eloquent\Collection && $relationData->count() === 0)){
                    $entity->addSimpleRelationship($relationKey, null);
                    continue;
                } else {
                    $relationData = isset($relationData) ? $relationData->toArray() : null;
                    $relationClass = EntitiesRelations::ENTITIES_CLASSES[$relationKey];
                    switch(static::ELOQUENT_RELATIONS[$relationKey]['type']){
                        case EntitiesRelations::GOT_ONE:
                            $relationEntity = new $relationClass;
                            foreach($relationClass::COLUMNS as $col){
                                $relationEntity->{$col} = $relationData[$col];
                            }
                            $entity->addSimpleRelationship($relationKey, $relationEntity);
                            break;
                        case EntitiesRelations::GOT_MANY:
                            foreach($relationData as $relation){
                                $relationEntity = new $relationClass;
                                foreach($relationClass::COLUMNS as $col){
                                    $relationEntity->{$col} = $relation[$col];
                                }
                                $entity->addArrayRelationships($relationKey, $relationEntity);
                            }
                    }
                }
            }
        }
    }

    protected function parseEloquentCollection($eloquentCollection)
    {
        $entityClass = static::ENTITY_CLASS;
        $result = [];
        while($eloquentCollection->count() > 0){
            $eloquentItem = $eloquentCollection->shift();
            $dataEntity = new $entityClass;
            foreach($entityClass::COLUMNS as $entityKey){
                $dataEntity->{$entityKey} = $eloquentItem->{$entityKey};
            }
            foreach($this->with as $relationKey){
                $relationClass = EntitiesRelations::ENTITIES_CLASSES[$relationKey];
                switch(static::ELOQUENT_RELATIONS[$relationKey]['type']){
                    case EntitiesRelations::GOT_ONE:
                        $relationEntity = new $relationClass;
                        $eloqItemRelation = $eloquentItem->{static::ELOQUENT_RELATIONS[$relationKey]['name']};
                        foreach($relationClass::COLUMNS as $col){
                            if(isset($eloqItemRelation->{$col})){
                                $relationEntity->{$col} = $eloqItemRelation->{$col};
                            }
                        }
                        $dataEntity->addSimpleRelationship($relationKey, $relationEntity);
                        break;
                    case EntitiesRelations::GOT_MANY:
                        $eloqItemRelations = $eloquentItem->{static::ELOQUENT_RELATIONS[$relationKey]['name']};
                        foreach($eloqItemRelations as $eloqItemRelation){
                            $relationEntity = new $relationClass;
                            foreach($relationClass::COLUMNS as $col){
                                if(isset($eloqItemRelation->{$col})){
                                    $relationEntity->{$col} = $eloqItemRelation->{$col};
                                }
                            }
                            $dataEntity->addArrayRelationships($relationKey, $relationEntity);
                        }
                        break;
                }
            }
            $result[] = $dataEntity;
        }
        return $result;
    }
}