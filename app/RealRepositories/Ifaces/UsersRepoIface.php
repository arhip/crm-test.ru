<?php

namespace App\RealRepositories\Ifaces;

use App\DataEntities\UserEntity;

interface UsersRepoIface extends AbstractRepoIface
{
    public function get(int $id) : ?UserEntity;
    public function add(UserEntity $entity) : UserEntity;
}
