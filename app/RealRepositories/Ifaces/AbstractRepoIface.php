<?php

namespace App\RealRepositories\Ifaces;


interface AbstractRepoIface
{
    public function with(string $key);
    public function del(array $ids);
}