<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppDependenciesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {


        $this->app->bind(\App\Service\Users\UsersServiceIface::class, function ($app) {
            return new \App\Service\Users\UsersService(
                new \App\RealRepositories\Eloquent\UsersRepo()
            );
        });

    }
}
