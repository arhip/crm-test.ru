<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">
<head>

        <title>{{ config('app.name') }}</title>

    <link rel="canonical" href="{{ URL::current() }}"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=1">
    <meta name="theme-color" content="#50b2ee">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700&amp;subset=cyrillic"
          type="text/css" crossorigin="crossorigin">
            <link rel="stylesheet" href="/css/app.css" type="text/css"/>
</head>

<body>

    <div class="wrapper" id="app">
            <div class="container-fluid pl-0 pr-0">
                @include('static/header')
                @yield('content')
            </div>
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>