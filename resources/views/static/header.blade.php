<header class="header fixed-top">
    <nav class="navbar navbar-expand-md">
        <div class="container-fluid p-0 justify-content-between" style = "background-color: #badaf7">
        <a href="/users"> Список клиентов</a>
        <a href="/interests" class="ml-3"> Список интересов</a>
            <div class="flex-grow-1"></div>
        </div>
        <button style="position: absolute;right: 1px;" id="toggle-navigation" class="navbar-toggler" aria-label="navigation toggle" type="button">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
    </nav>
</header>
