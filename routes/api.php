<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('interests')->group(function () {
    Route::get('list', 'InterestsController@getList');
    Route::put('{interest}', 'InterestsController@update');
    Route::delete('{interest}', 'InterestsController@delete');
    Route::post('create', 'InterestsController@create');
});

Route::prefix('users')->group(function () {
    Route::get('list', 'UsersController@getUsersList');
    Route::delete('{user}', 'UsersController@deleteUser');
    Route::put('{user}', 'UsersController@updateUser');
    Route::post('create', 'UsersController@createUser');
});



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
